package tfStateBuckets

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"math"
	"os"
	"strings"
	"time"

	"github.com/pulumi/pulumi-gcp/sdk/v6/go/gcp/serviceaccount"
	"github.com/pulumi/pulumi-gcp/sdk/v6/go/gcp/storage"
	vaultGeneric "github.com/pulumi/pulumi-vault/sdk/v4/go/vault/generic"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi/config"
)

type StateBucket struct {
	GCPProject       string
	Bucket           string
	VaultAddress     string
	VaultSecretPath  string
	MinRotationDays  int
	Protect          bool
	ServiceAccountID string
	//
	rwLabel string
	fake    bool
}

func Ensure(ctx *pulumi.Context) error {
	cfg := config.New(ctx, "")
	b := &StateBucket{
		GCPProject:       cfg.Require("GCPProject"),
		Bucket:           cfg.Get("Bucket"),
		VaultSecretPath:  cfg.Get("VaultSecretPath"),
		ServiceAccountID: cfg.Get("ServiceAccountID"),
		Protect:          cfg.GetBool("Protect"),
		fake:             cfg.GetBool("fake"),
	}
	return b.ensure(ctx)
}

func (o *StateBucket) ensure(ctx *pulumi.Context) error {
	if o.fake {
		return nil
	}

	o.init(ctx)

	bucket, err := o.ensureBucket(ctx)
	if err != nil {
		return err
	}

	svcAccount, svcAccountKey, err := o.ensureBucketServiceAccount(ctx, bucket)
	if err != nil {
		return err
	}

	err = o.ensureBucketServiceAccountPermissions(ctx, bucket, svcAccount)
	if err != nil {
		return err
	}

	// ensure base64 decoded key is in vault..
	_ = svcAccountKey.PrivateKey.ApplyT(func(base64Kubeconfig string) (string, error) {
		decoded, err := base64.StdEncoding.DecodeString(base64Kubeconfig)
		if err != nil {
			return "", err
		}
		s := string(decoded)
		return s, o.ensureKeyInVault(ctx, svcAccountKey, s)
	}).(pulumi.StringOutput)

	o.exportData(ctx, bucket, svcAccount, svcAccountKey)

	return nil
}

func (o *StateBucket) init(ctx *pulumi.Context) {
	// set if not already set
	empty := &StateBucket{}

	if o.Bucket == empty.Bucket {
		o.Bucket = strings.Replace(
			fmt.Sprintf("%v%v", os.Getenv("BUCKET_PREFIX"), ctx.Stack()),
			".", "-", -1)
	}

	if o.ServiceAccountID == empty.ServiceAccountID {
		o.ServiceAccountID = strings.Replace(ctx.Stack(), ".", "-", -1)
	}

	if o.MinRotationDays == empty.MinRotationDays {
		o.MinRotationDays = 1 // TODO For testing. Set better default.
	}

	if o.VaultAddress == empty.VaultAddress {
		o.VaultAddress = "https://live01-ncsa.vault.ubisoft.com:8200"
	}

	if o.VaultSecretPath == empty.VaultSecretPath {
		o.VaultSecretPath = fmt.Sprintf("kv/buckets_manager/%v", ctx.Stack())
	}
}

func (o *StateBucket) ensureBucket(ctx *pulumi.Context) (*storage.Bucket, error) {
	return storage.NewBucket(ctx, o.Bucket, &storage.BucketArgs{
		ForceDestroy:             pulumi.Bool(false),
		Location:                 pulumi.String("US"),
		UniformBucketLevelAccess: pulumi.Bool(false),
		Name:                     pulumi.String(o.Bucket),
		Project:                  pulumi.String(o.GCPProject),
		PublicAccessPrevention:   pulumi.String("enforced"),
		Versioning: storage.BucketVersioningArgs{
			Enabled: pulumi.Bool(true),
		},
		LifecycleRules: storage.BucketLifecycleRuleArray{
			storage.BucketLifecycleRuleArgs{
				Action: storage.BucketLifecycleRuleActionArgs{
					Type: pulumi.String("Delete"),
				},
				Condition: storage.BucketLifecycleRuleConditionArgs{
					NumNewerVersions: pulumi.Int(12),
					WithState:        pulumi.String("ARCHIVED"),
				},
			},
			storage.BucketLifecycleRuleArgs{
				Action: storage.BucketLifecycleRuleActionArgs{
					Type: pulumi.String("Delete"),
				},
				Condition: storage.BucketLifecycleRuleConditionArgs{
					DaysSinceNoncurrentTime: pulumi.Int(24),
				},
			},
		},
	}, pulumi.Protect(o.Protect))
}

func (o *StateBucket) ensureBucketServiceAccount(ctx *pulumi.Context, bucket *storage.Bucket) (
	acct *serviceaccount.Account,
	acctKey *serviceaccount.Key,
	err error,
) {
	acct, err = serviceaccount.NewAccount(ctx, o.ServiceAccountID, &serviceaccount.AccountArgs{
		AccountId:   pulumi.String(o.ServiceAccountID),
		DisplayName: pulumi.String(fmt.Sprintf("Service account for bucket %v %v", o.Bucket, o.rwLabel)),
		Project:     pulumi.String(o.GCPProject),
	})
	if err != nil {
		return nil, nil, err
	}

	// minimum days before a rotation automatically happens (TODO Set to higher number when ready)
	timeStart := time.Date(2000, time.Month(1), 1, 0, 0, 0, 0, time.UTC)
	timeNow := time.Now()
	rotationPeriod := math.Round(timeNow.Sub(timeStart).Hours() / 24 / float64(o.MinRotationDays))

	acctKey, err = serviceaccount.NewKey(ctx, "bucket-key", &serviceaccount.KeyArgs{
		ServiceAccountId: acct.Name,
		Keepers: pulumi.ToMap(map[string]interface{}{
			"time-rotation": rotationPeriod, // every time the period changes, the key should get rewritten (TODO validate)
		}),
	})
	if err != nil {
		return
	}

	return
}

func (o *StateBucket) ensureBucketServiceAccountPermissions(
	ctx *pulumi.Context,
	bucket *storage.Bucket,
	acct *serviceaccount.Account,
) (err error) {
	_, err = storage.NewBucketIAMBinding(ctx, "iam-binding", &storage.BucketIAMBindingArgs{
		Bucket: bucket.Name,
		Role:   pulumi.String("roles/storage.objectAdmin"),
		Members: pulumi.StringArray{
			pulumi.Sprintf("serviceAccount:%v", acct.Email),
		},
	})
	if err != nil {
		return err
	}

	return nil
}

func (o *StateBucket) ensureKeyInVault(
	ctx *pulumi.Context,
	acctKey *serviceaccount.Key,
	decodedKey string,
) (err error) {
	jsonData, err := json.Marshal(map[string]interface{}{
		"credentials": decodedKey,
	})
	if err != nil {
		return err
	}

	_, err = vaultGeneric.NewSecret(ctx, "vault-secret", &vaultGeneric.SecretArgs{
		Path:     pulumi.String(o.VaultSecretPath),
		DataJson: pulumi.String(jsonData),
	})
	if err != nil {
		return err
	}

	return nil
}

func (o *StateBucket) exportData(
	ctx *pulumi.Context,
	bucket *storage.Bucket,
	acct *serviceaccount.Account,
	acctKey *serviceaccount.Key,
) (err error) {
	ctx.Export("bucket", bucket.Name)
	ctx.Export("gcp_project", pulumi.String(o.GCPProject))
	ctx.Export("vault_addr", pulumi.String(os.Getenv("VAULT_ADDR")))
	ctx.Export("vault_namespace", pulumi.String(os.Getenv("VAULT_NAMESPACE")))
	ctx.Export("vault_secret_path", pulumi.String(o.VaultSecretPath))
	ctx.Export("vault_secret_field", pulumi.String("credentials"))
	return nil
}
